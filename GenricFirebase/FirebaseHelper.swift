//
//  FirebaseHelper.swift
//  GenricFirebase
//
//  Created by Ashish Bhardwaj on 13/02/19.
//  Copyright © 2019 sachtechsolution. All rights reserved.
//

import Foundation
import Firebase

class FirebaseHelper
{
    private static var firebasehelper:FirebaseHelper!
    class func share() -> FirebaseHelper{
        if firebasehelper == nil {
            firebasehelper = FirebaseHelper()
        }
        return firebasehelper
    }
   
    func auth(usedfor:String,email:String,password:String,completion:@escaping (String,User)->Void)
    {
        if usedfor == "login"{
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error == nil{
             guard let userdata = user?.user else { return }
                completion("Successfully Logged in",userdata)
            }
            else
            {
                print(error?.localizedDescription as Any)
                 guard let userdata = user?.user else { return }
                completion((error?.localizedDescription)!,userdata)
            }
            
            }}
        else
        {
            Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
                if error == nil{
                    guard let user = authResult?.user else { return }
                    completion("Succesfully Registered",user)
                }
                else
                {
                    print(error?.localizedDescription as Any)
                    guard let user = authResult?.user else { return }
                    completion((error?.localizedDescription)!,user)
                }
            }
        }
        
    }
    
    func sendOtp(mobileno:String,completion:@escaping (String,String)->Void)
    {
        let mobile = "+91\(mobileno)"
        PhoneAuthProvider.provider().verifyPhoneNumber( mobile , uiDelegate: nil) { (verificationID, error) in
            if  error == nil
            {
                let verificationId = verificationID
                completion("Otp Sent",verificationId!)
            }
            else
            {
                guard let verificationid = verificationID else { return }
                completion((error?.localizedDescription)!,verificationid)
            }
            
        }
    }
    
    
    func verifyOtp(verificationId:String,otp:String,completion:@escaping (String,User)->Void)
    {
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationId,
            verificationCode: otp)
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if error == nil {
                  guard let user = authResult?.user else { return }
              completion("Verified Succesfully",user)
            }
            else
            {
                guard let user = authResult?.user else { return }
                completion((error?.localizedDescription)!,user)
            }
        }
    }
    
    

    
    func signout()
    {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    
}
