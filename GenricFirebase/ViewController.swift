//
//  ViewController.swift
//  GenricFirebase
//
//  Created by Ashish Bhardwaj on 13/02/19.
//  Copyright © 2019 sachtechsolution. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var verifybtn: UIButton!
    @IBOutlet weak var otpTextFeild: UITextField!
    @IBOutlet weak var sendotp_btn: UIButton!
    @IBOutlet weak var mobileTextfeild: UITextField!
    @IBOutlet weak var pwdTextfeild: UITextField!
    @IBOutlet weak var emailTextfeild: UITextField!
      let defaults:UserDefaults = UserDefaults.standard
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        otpTextFeild.isHidden = true
        verifybtn.isHidden = true
        
      
    }
    @IBAction func login(_ sender: Any) {
        
        if (emailTextfeild.text?.isEmpty)! || (pwdTextfeild.text?.isEmpty)!
        {
            ToastView.shared.short(self.view, txt_msg: "feilds can't ne empty")
        }
        else if isValidEmail(testStr: emailTextfeild.text!) == false
        {
            ToastView.shared.short(self.view, txt_msg: "Email is not Valid")
        }
        else
        {
            FirebaseHelper.share().auth(usedfor: "login", email: emailTextfeild.text!, password: pwdTextfeild.text!)
            {(status,response) in
                print(response.email!)
                // let email = response.email
                // let uid = response.uid
                ToastView.shared.short(self.view, txt_msg: status)
            }
        }
        
    }
    @IBAction func signup(_ sender: Any) {
        
        if (emailTextfeild.text?.isEmpty)! || (pwdTextfeild.text?.isEmpty)!
        {
            ToastView.shared.short(self.view, txt_msg: "feilds can't ne empty")
        }
        else if isValidEmail(testStr: emailTextfeild.text!) == false
        {
            ToastView.shared.short(self.view, txt_msg: "Email is not Valid")
        }
        else
        {
            FirebaseHelper.share().auth(usedfor: "signup", email: emailTextfeild.text!, password: pwdTextfeild.text!)
            {(status,response) in
                print(response)
               // let email = response.email
               // let uid = response.uid
                ToastView.shared.short(self.view, txt_msg: status)

            }
        }
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    @IBAction func sendotp(_ sender: Any) {
        if (mobileTextfeild.text?.count)! < 10
        {
            ToastView.shared.short(self.view, txt_msg: "Enter Valid mobile no.")
        }
        else{
        FirebaseHelper.share().sendOtp(mobileno: mobileTextfeild.text!)
        {
            (status,verificationId) in
            if status == "Otp Sent"
            {
                self.defaults.set(verificationId, forKey: "verificationId")
                self.defaults.synchronize()
                 ToastView.shared.short(self.view, txt_msg: status)
                self.mobileTextfeild.isHidden = true
                self.sendotp_btn.isHidden = true
                self.verifybtn.isHidden = false
                self.otpTextFeild.isHidden = false
            }
            else{
            
            ToastView.shared.short(self.view, txt_msg: status)
            }
            
            }
        }
        
    }
    
    @IBAction func verifyOtp(_ sender: Any) {
        if (otpTextFeild.text?.count)! < 6
        {
            ToastView.shared.short(self.view, txt_msg: "Invalid Otp")
        }
        else
        {
             guard let id = self.defaults.string(forKey: "verificationId") else{return}
            FirebaseHelper.share().verifyOtp(verificationId: id, otp: otpTextFeild.text!)
            {
                (status,response) in
                
                ToastView.shared.short(self.view, txt_msg: status)
            }
        }
    }
    
    
}

